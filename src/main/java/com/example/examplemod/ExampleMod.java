package com.example.examplemod;
//
//import versatile.api.IMod;
//import versatile.api.game.OptionalItemInfo;
//import versatile.api.game.block.BlockHandler;
//import versatile.api.game.block.BlockInfo;
//import versatile.api.game.block.BlockInitializer;
//import versatile.api.game.block.VanillaBlock;
//import versatile.api.game.entity.Hand;
//import versatile.api.game.entity.world.PlayerContainer;
//import versatile.api.game.inventory.ArrayInventory;
//import versatile.api.game.item.ItemContainer;
//import versatile.api.game.item.ItemInitializer;
//import versatile.api.game.world.Pos;
//import versatile.api.game.world.Updatable;
//import versatile.api.game.world.WorldController;
//import versatile.api.mod.InfoEnvironment;
//import versatile.api.mod.ModInfo;
//
//import java.net.URL;
//
////@Modification("example")
//public class ExampleMod implements IMod {
//    private static BlockInfo TEST2;
//
//
//    enum Items implements OptionalItemInfo {
//        KEY("key");
//
//        private String name;
//        private String displayName;
//        private String textureName;
//        private boolean lang;
//
//        public BlockInfo tempInfo;
//
//        Items(String name) {
//            this(name, name, name, false);
//        }
//
//        Items(String name, String displayName, String textureName, boolean lang) {
//            this.name = name;
//            this.displayName = displayName;
//            this.textureName = textureName;
//            this.lang = lang;
//        }
//
//        public static void register(ItemInitializer initializer) {
//            for (Items item : Items.values()) {
//                item.tempInfo = initializer.registerItem(factory -> {
//                    factory.setUniqueName(item.name);
//                    if (item.lang)
//                        factory.setDisplayName(lang -> lang.fromLangProperties(item.displayName));
//                    else
//                        factory.setDisplayName(lang -> lang.permanentFormatted(item.displayName));
//                    factory.setTexture(manager ->
//                            manager.loadTextureFromResource(item.textureName));
//                });
//
//            }
//        }
//
//        @Override
//        public BlockInfo orEmpty() {
//            return tempInfo;
//        }
//    }
//
//    @Override
//    public ModInfo createModInfo(InfoEnvironment environment) {
//        return environment.createInfo(
//                "Example mod",
//                "1.0",
//                lang -> lang.fromLangProperties("mod.description"),
//                "lionblazer"
//        );
//    }
//
//
//    @Override
//    public void createItems(ItemInitializer initializer) {
//        Items.register(initializer);
//    }
//
//    @Override
//    public void createBlocks(BlockInitializer initializer) {
//        TEST2 = initializer.registerDataBlock(factory -> {
//            factory.setUniqueName("Test2".trim().replace(' ', '_').toLowerCase());
//            factory.setDisplayName(lang -> lang.permanent("Test2"));
//            factory.setTexture(manager -> manager.loadTextureFromURL(new URL("https://sun9-13.userapi.com/c854020/v854020855/caf7e/EJF_ravVaoc.jpg")));
//        }, TestDataCapacitor::new);
//    }
//
//
//    private static class TestDataCapacitor implements ArrayInventory, Updatable, BlockHandler {
//        private ItemContainer[] container = new ItemContainer[27];
//
//        @Override
//        public ItemContainer[] getItems() {
//            return container;
//        }
//
//
//        @Override
//        public void update(WorldController world, Pos center) {
//            ItemContainer key = getItem(0);
//            if (Items.KEY.is(key)) {
//                for (int i = 0; i < 256; i++) {
//                    int curY = i + 1;
//                    if(center.y() + curY > 256)
//                        return;
//                    Pos pos = center.offsetY(curY);
//
//                    if(VanillaBlock.COBBLESTONE.is(world.getBlock(pos)))
//                       continue;
//                    world.setBlock(VanillaBlock.COBBLESTONE, pos);
//
//                    break;
//                }
//
//                key.increase(-1);
//            }
//        }
//
//        @Override
//        public int updatePeriod() {
//            return 5;
//        }
//
//        @Override
//        public boolean blockActivated(WorldController world, Pos center, PlayerContainer player, Hand hand) {
//            if (player.getHeldItem(hand).isItemEqual(Items.KEY))
//                player.openInventory(this);
//            else
//                player.viewInventory(this);
//
//            return true;
//        }
//    }
//}
//
//
//
