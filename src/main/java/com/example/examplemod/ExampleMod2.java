package com.example.examplemod;

//
////@Modification("example")
//public class ExampleMod2 implements IMod {
//    private static final String imageURL = "https://sun9-29.userapi.com/c854020/v854020855/caf7e/EJF_ravVaoc.jpg";
//    private static BlockInfo TEST2;
//
//
//    enum Blocks implements OptionalBlockInfo {
//        COPPER_ORE("copper_ore", HarvestType.PICKAXE, 1, 1f),
//        TIN_ORE("tin_ore", HarvestType.PICKAXE, 1, 1.2f),
//        ;
//
//        private String name;
//        private String displayName;
//        private String textureName;
//        private HarvestType harvestType;
//        private int harvestTier;
//        private boolean lang;
//        private float hardness;
//
//        public BlockInfo tempInfo;
//
//        Blocks(String name) {
//            this(name, name, name, HarvestType.NONE, 0, false, 0.1f);
//        }
//
//        Blocks(String name, HarvestType harvestType, int harvestTier, float hardness) {
//            this(name, name, name, harvestType, harvestTier, false, hardness);
//        }
//
//        Blocks(String name, String displayName, String textureName, HarvestType harvestType, int harvestTier, boolean lang, float hardness) {
//            this.name = name;
//            this.displayName = displayName;
//            this.textureName = textureName;
//            this.harvestType = harvestType;
//            this.harvestTier = harvestTier;
//            this.lang = lang;
//            this.hardness = hardness;
//        }
//
//        @Override
//        public BlockInfo orEmpty() {
//            return tempInfo;
//        }
//
//        private static class OreCapacitor implements DataCapacitor {
//            private int oreCount = 10;
//
//            @Override
//            public void restoreDataFrom(TagCompound tag) {
//                oreCount = tag.getIntOrDefault("oreCount", oreCount);
//            }
//
//            @Override
//            public void saveDataTo(TagCompound tag) {
//                tag.set("oreCount", oreCount);
//            }
//        }
//
//        public static void register(BlockInitializer initializer) {
//            for (Blocks block : Blocks.values()) {
//                block.tempInfo = initializer.registerDataBlock(factory -> {
//                    factory.setUniqueName(block.name);
//                    factory.setHarvestType(block.harvestType);
//                    factory.setHarvestTier(block.harvestTier);
//                    factory.setHardness(block.hardness);
//                    if (block.lang)
//                        factory.setDisplayName(lang -> lang.fromLangProperties(block.displayName));
//                    else
//                        factory.setDisplayName(lang -> lang.permanentFormatted(block.displayName));
//                    factory.setTexture(manager ->
//                            manager.loadTextureFromResource(block.textureName));
//                }, OreCapacitor::new);
//
//            }
//        }
//    }
//
//    enum Items implements OptionalItemInfo {
//        PROCESSOR("processor"),
//        LOGIC_BLOCK("logic_block"),
//        ;
//
//        private String name;
//        private String displayName;
//        private String textureName;
//        private boolean lang;
//
//        public BlockInfo tempInfo;
//
//        Items(String name) {
//            this(name, name, name, false);
//        }
//
//        Items(String name, String displayName, String textureName, boolean lang) {
//            this.name = name;
//            this.displayName = displayName;
//            this.textureName = textureName;
//            this.lang = lang;
//        }
//
//        public static void register(ItemInitializer initializer) {
//            for (Items item : Items.values()) {
//                item.tempInfo = initializer.registerItem(factory -> {
//                    factory.setUniqueName(item.name);
//                    if (item.lang)
//                        factory.setDisplayName(lang -> lang.fromLangProperties(item.displayName));
//                    else
//                        factory.setDisplayName(lang -> lang.permanentFormatted(item.displayName));
//                    factory.setTexture(manager ->
//                            manager.loadTextureFromResource(item.textureName));
//                });
//
//            }
//        }
//
//        @Override
//        public BlockInfo orEmpty() {
//            return tempInfo;
//        }
//    }
//
//    @Override
//    public ModInfo createModInfo(InfoEnvironment environment) {
//        return environment.createInfo(
//                "Example mod",
//                "1.0",
//                lang -> lang.fromLangProperties("mod.description"),
//                "lionblazer"
//        );
//    }
//
//    @Override
//    public void createItems(ItemInitializer initializer) {
//        Items.register(initializer);
//
//    }
//
//    @Override
//    public void createBlocks(BlockInitializer initializer) {
//        Blocks.register(initializer);
//        TEST2 = initializer.registerDataBlock(factory -> {
//            factory.setUniqueName("Test2".trim().replace(' ', '_').toLowerCase());
//            factory.setDisplayName(lang -> lang.permanent("Test2"));
//            factory.setTexture(manager -> manager.loadTextureFromURL(new URL("https://sun9-13.userapi.com/c854020/v854020855/caf7e/EJF_ravVaoc.jpg")));
//        }, TestDataCapacitor::new);
//    }
//
//    @Override
//    public void createBlockHandlers(BlockHandlerInitializer initializer) {
//        initializer.registerHandler(new BlockHandler() {
//            @Override
//            public boolean breakBlock(WorldController world, Pos pos) {
//                Blocks.OreCapacitor capacitor = world.getBlockData(pos);
//                if (capacitor.oreCount >= 0) {
//                    capacitor.oreCount--;
//                    return false;
//                } else
//                    return true;
//            }
//        }, Blocks.values());
//
//
//        initializer.registerHandler(new BlockHandler() {
//            @RequiredDependencies(DIRT)
//            @Override
//            public boolean blockActivated(WorldController world, Pos center, PlayerContainer player, Hand hand) {
//                world.setBlock(DIRT, center);
//                return true;
//            }
//        }, COBBLESTONE);
//
//        initializer.registerHandler(new BlockHandler() {
//            @Override
//            public boolean blockActivated(WorldController world, Pos center, PlayerContainer player, Hand hand) {
//                if (!world.side().isClient()) {
//                    TestDataCapacitor capacitor = world.getBlockData(center);
//                    player.openInventory(capacitor);
//                }
//                return true;
//            }
//        }, TEST2);
//    }
//
//
//    private static class TestDataCapacitor implements ArrayInventory {
//        private ItemContainer[] container = new ItemContainer[26];
//
//        @Override
//        public ItemContainer[] getItems() {
//            return container;
//        }
//    }
//
//
//    @Override
//    public void createEntityHandlers(EntityHandlerInitializer initializer) {
////        initializer.registerHandler(VanillaEntity.ZOMBIE, new EntityHandler<EntityZombieContainer>() {
////            @Override
////            public boolean entityJoinWorld(WorldController world, EntityZombieContainer container) {
////                Pos pos = container.getPosition().expandY(2).clampToPos();
////                world.setBlock(COBBLESTONE, pos);
////                return true;
////            }
////        });
//    }
//}
//
//
//
