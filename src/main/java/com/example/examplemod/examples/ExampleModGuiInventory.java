package com.example.examplemod.examples;

import versatile.api.IMod;
import versatile.api.Modification;
import versatile.api.game.block.BlockHandler;
import versatile.api.game.block.BlockHandlerInitializer;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.VanillaBlock;
import versatile.api.game.entity.Hand;
import versatile.api.game.entity.world.PlayerContainer;
import versatile.api.game.inventory.ArrayInventory;
import versatile.api.game.item.ItemContainer;
import versatile.api.game.item.VanillaItem;
import versatile.api.game.item.property.ColorType;
import versatile.api.game.world.Pos;
import versatile.api.game.world.WorldController;

@Modification("example_basic_gui_inventory")
public class ExampleModGuiInventory implements IMod {
    @Override
    public void createBlockHandlers(BlockHandlerInitializer initializer) {
        initializer.registerHandler(new BlockPlaceHandler(), VanillaBlock.MAGMA);
    }

    private static class BlockPlaceHandler implements BlockHandler {
        @Override
        public boolean blockActivated(WorldController world, Pos pos, PlayerContainer clicker, Hand clickHand) {
            if (!clicker.getHeldItem(Hand.MAIN).isEmpty())
                return false;

            ArrayInventory.Basic basic = new ArrayInventory.Basic(9);

            for (int i = 0; i < basic.size(); i++) {
                basic.setItem(i, new ItemContainer(VanillaItem.DYE.with(ColorType.values()[i]), 16));
            }

            clicker.openInventory(basic, setting -> {
                setting.overrideSlotsInfo(slot -> slot.setMask(i -> false));
                setting.makeLines(9);
            });
            world.setBlock(BlockInfo.EMPTY, pos);
            return true;
        }
    }

}
