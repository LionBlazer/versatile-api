package com.example.examplemod.examples;

import versatile.api.IMod;
import versatile.api.Modification;
import versatile.api.game.block.BlockInitializer;
import versatile.api.game.block.data.BlockDataHandler;
import versatile.api.game.block.data.TagCompound;
import versatile.api.game.entity.Hand;
import versatile.api.game.entity.world.PlayerContainer;
import versatile.api.game.item.ItemContainer;
import versatile.api.game.item.VanillaItem;
import versatile.api.game.world.Pos;
import versatile.api.game.world.WorldController;

import java.awt.*;

@Modification("example_data_block")
public class ExampleModDataBlock implements IMod {

    @Override
    public void createBlocks(BlockInitializer initializer) {
        initializer.registerDataBlock("simple_clicker", data -> {
            data.setTexture(factory -> factory.loadTextureFromColor(Color.RED));
            data.setDisplayName(manager -> manager.permanent("Кликер"));
        }, TestCapacitor::new);
    }

    public static class TestCapacitor implements BlockDataHandler {
        private int value;

        @Override
        public boolean blockActivated(WorldController world, Pos pos, PlayerContainer clicker, Hand clickHand) {
            value++;
            if (value % 5 == 0)
                world.spawnEntity(VanillaItem.BLAZE_ROD, pos.as3d().offset(0.5, 1.5, 0.5));
            return true;
        }

        @Override
        public boolean breakBlock(WorldController world, Pos pos) {
            ItemContainer container = new ItemContainer(VanillaItem.BLAZE_POWDER, value);
            world.spawnEntity(container, pos.as3d().offset(0.5, 0.5, 0.5));
            return true;
        }

        @Override
        public void restoreDataFrom(TagCompound tag) {
            value = tag.getIntOrDefault("value", 0);
        }

        @Override
        public void saveDataTo(TagCompound tag) {
            tag.set("value", value);
        }
    }
}
