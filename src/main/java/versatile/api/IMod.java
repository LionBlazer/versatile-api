package versatile.api;

import versatile.api.game.block.BlockHandlerInitializer;
import versatile.api.game.block.BlockInitializer;
import versatile.api.game.entity.EntityHandlerInitializer;
import versatile.api.game.item.ItemInitializer;
import versatile.api.mod.InfoEnvironment;
import versatile.api.mod.ModInfo;

public interface IMod {
    default ModInfo createModInfo(InfoEnvironment environment){
        return environment.createInfo("Mod");
    }

    default void createBlocks(BlockInitializer initializer){

    }

    default void createBlockHandlers(BlockHandlerInitializer initializer){

    }

    default void createEntityHandlers(EntityHandlerInitializer initializer){

    }

    default void createItems(ItemInitializer initializer){
    }
}
