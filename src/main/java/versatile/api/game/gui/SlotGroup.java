package versatile.api.game.gui;

import org.apache.commons.lang3.ArrayUtils;
import versatile.api.game.block.data.DataCapacitor;
import versatile.api.game.block.data.TagCompound;
import versatile.api.game.inventory.Inventory;

import java.util.Objects;

public class SlotGroup implements DataCapacitor {
    private transient Inventory inventory;
    private int[] indices;

    public SlotGroup(Inventory inventory) {
        this.inventory = Objects.requireNonNull(inventory);

        this.indices = new int[inventory.size()];

        for (int i = 0; i < inventory.size(); i++)
            this.indices[i] = i;
    }

    public SlotGroup(Inventory inventory, int... indices) {
        this.inventory = Objects.requireNonNull(inventory);
        this.indices = Objects.requireNonNull(indices);
    }

    public SlotGroup subGroup(int from, int to) {
        return new SlotGroup(inventory, ArrayUtils.subarray(indices, from, to));
    }


    public int[] getIndices() {
        return indices;
    }

    public Inventory getInventory() {
        return inventory;
    }

    @Override
    public void restoreDataFrom(TagCompound tag) {
        tag.set("indices", indices);
    }

    @Override
    public void saveDataTo(TagCompound tag) {
        indices = tag.getIntsOrEmpty("indices");
    }
}
