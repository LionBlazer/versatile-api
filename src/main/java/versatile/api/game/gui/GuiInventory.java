package versatile.api.game.gui;

import versatile.api.game.block.data.DataCapacitor;
import versatile.api.game.block.data.TagCompound;
import versatile.api.game.inventory.Inventory;
import versatile.api.internal.INGuiUtil;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Consumer;

public class GuiInventory implements DataCapacitor{
    /** Slot's container, notnull*/
    private transient Inventory inventory;

    private GuiSlot[] slots;
    private boolean canInteract = true;
    private SlotGroup transferGroup;

    public GuiInventory(Inventory inventory) {
        this.inventory = Objects.requireNonNull(inventory);
        slots = new GuiSlot[inventory.size()];
        makeLines(9);
        transferGroup = new SlotGroup(inventory);
    }

    public void makeLines(int length) {
        final float slotSize = INGuiUtil.getDefaultSlotSize();
        final int offset = 0;
        for (int i = 0; i < inventory.size(); i++) {
            float slotX = 8 + offset + (slotSize + offset) * (i % length);
            float slotY = 8 + offset + (float) (i / length) * (slotSize + offset + 2);
            if(slots[i] == null)
                slots[i] = new GuiSlot(slotX, slotY, i, inventory);
            else {
                slots[i].setX(slotX);
                slots[i].setY(slotY);
            }
        }
    }

    /**
     * Overrides gui slots info
     * @param group those slots that will be redesigned
     * @param configure slot's redesign consumer
     */
    public void overrideSlotsInfo(SlotGroup group, Consumer<GuiSlot> configure) {
        if(group.getInventory() != inventory)
            throw new IllegalArgumentException("Slot group was linked with another inventory");
        for(int i : group.getIndices())
            configure.accept(slots[i]);
    }

    /**
     * Overrides gui all slots info
     * @param configure slot's redesign consumer
     */
    public void overrideSlotsInfo(Consumer<GuiSlot> configure) {
        final SlotGroup group = new SlotGroup(inventory);
        for(int i : group.getIndices())
            configure.accept(slots[i]);
    }

    public void setNoInteract() {
        canInteract = false;
    }

    public boolean canInteract(){
        return canInteract;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public SlotGroup getTransferGroup() {
        return transferGroup;
    }

    public GuiSlot[] slots() {
        return Arrays.copyOf(slots, slots.length);
    }

    public int countVisibleSlots(){
        int i = 0;
        for (GuiSlot slot: slots)
            if (slot.isVisible())
                i++;
        return i;
    }

    @Override
    public void restoreDataFrom(TagCompound tag) {
        canInteract = tag.getBooleanOrDefault("can_interact", true);
        transferGroup = new SlotGroup(inventory, tag.getIntsOrEmpty("transfer_group"));
        int size = tag.getIntOrDefault("slot_count", 0);

        slots = new GuiSlot[size];
        for (int i = 0; i < size; i++) {
            TagCompound sub = tag.getTagOrEmpty("slot_" + i);

            slots[i] = new GuiSlot(inventory);
            slots[i].restoreDataFrom(sub);
        }
    }

    @Override
    public void saveDataTo(TagCompound tag) {
        tag.set("can_interact", canInteract);
        tag.set("slot_count", slots.length);
        tag.set("transfer_group", transferGroup.getIndices());

        for (int i = 0; i < slots.length; i++) {
            GuiSlot slot = slots[i];

            TagCompound sub = slot.serialize();

            tag.set("slot_" + i, sub);
        }
    }
}
