package versatile.api.game.inventory;

import versatile.api.game.item.ItemContainer;

public interface ArrayInventory extends Inventory {
    /**
     * @return cached item array for inventory, might contains a null-elements
     */
    ItemContainer[] getItems();

    @Override
    default int size() {
        return getItems().length;
    }

    @Override
    default ItemContainer getItem(int i) {
        ItemContainer item = getItems()[i];
        return item != null ? item : ItemContainer.EMPTY;
    }

    @Override
    default ItemContainer setItem(int i, ItemContainer item) {
        ItemContainer previous = getItems()[i];
        getItems()[i] = item.clone();
        return previous;
    }

    @Override
    default ItemContainer[] collectInArray() {
        return prepareArray(getItems());
    }

    /**
     * Replaces all null-elements to {@link ItemContainer#EMPTY} in {@code array}
     *
     * @return new array
     */
    static ItemContainer[] prepareArray(ItemContainer[] array) {
        ItemContainer[] newArray = new ItemContainer[array.length];
        for (int i = 0; i < array.length; i++)
            if (array[i] == null)
                newArray[i] = ItemContainer.EMPTY;
            else newArray[i] = array[i];
        return newArray;
    }

    final class Basic implements ArrayInventory {
        private final ItemContainer[] contained;

        public Basic(int size) {
            this.contained = new ItemContainer[size];
        }

        @Override
        public ItemContainer[] getItems() {
            return contained;
        }
    }
}
