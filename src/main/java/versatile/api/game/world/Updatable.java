package versatile.api.game.world;

public interface Updatable {
    default int updatePeriod(){
        return 1;
    }

    void update(WorldController world, Pos center);
}
