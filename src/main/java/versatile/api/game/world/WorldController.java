package versatile.api.game.world;

import versatile.api.game.GameSide;
import versatile.api.game.OptionalItemInfo;
import versatile.api.game.entity.EntityContainer;
import versatile.api.game.item.ItemContainer;

import java.util.UUID;

public interface WorldController extends BlockAccessor {
    GameSide side();


    UUID spawnEntity(EntityContainer object);
    UUID spawnEntity(ItemContainer object, Pos3d pos);

    default UUID spawnEntity(OptionalItemInfo item, Pos3d pos) {
        return spawnEntity(new ItemContainer(item), pos);
    }

    EntityContainer getEntity(UUID uuid);
    EntityContainer[] getAllEntities(double radius);
}
