package versatile.api.game.world;

import versatile.api.game.OptionalBlockInfo;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.data.DataCapacitor;

public interface BlockAccessor {
    void setBlock(BlockInfo block, Pos pos);
    BlockInfo getBlock(Pos pos);

    <T extends DataCapacitor> T getBlockData(Pos pos);

    default void setBlock(OptionalBlockInfo block, Pos initialPos,  Pos... pos){
        BlockInfo blockInfo = block.orEmpty();
        if(!blockInfo.isEmpty())
            setBlock(blockInfo, initialPos, pos);
    }

    default void setBlock(BlockInfo block, Pos initialPos, Pos... positions){
        setBlock(block, initialPos);
        for (Pos pos : positions)
            setBlock(block, pos);
    }

    default BlockInfo[] getBlocks(Pos initialPos, Pos... pos){
        BlockInfo[] infos = new BlockInfo[pos.length];
        infos[0] = getBlock(initialPos);
        for (int i = 1; i < pos.length + 1; i++)
            infos[i] = getBlock(pos[i]);
        return infos;
    }
}