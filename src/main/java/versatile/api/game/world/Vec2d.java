package versatile.api.game.world;

public class Vec2d {
    private double x;
    private double y;

    public Vec2d(Number x, Number y) {
        this.x = x.doubleValue();
        this.y = y.doubleValue();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
