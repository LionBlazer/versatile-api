package versatile.api.game.world;

public final class Pos3d {
    private final double x, y, z;

    public Pos3d(Number x, Number y, Number z) {
        this.x = x.doubleValue();
        this.y = y.doubleValue();
        this.z = z.doubleValue();
    }

    public Pos3d() {
        x = y = z = 0;
    }

    public Pos3d offset(Number x, Number y, Number z){
        return new Pos3d(x() + x.doubleValue(), y() + y.doubleValue(), z() + z.doubleValue());
    }

    public Pos3d offsetX(Number x){
        return offset(x, 0, 0);
    }

    public Pos3d offsetY(Number y){
        return offset(0, y, 0);
    }

    public Pos3d offsetZ(Number z){
        return offset(0, 0, z);
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }

    public double z() {
        return z;
    }

    public Pos clampToPos(){
        return new Pos(x, y, z);
    }
}
