package versatile.api.game.world;

public final class Pos {
    private final int x, y, z;

    public Pos(Number x, Number y, Number z) {
        this.x = x.intValue();
        this.y = y.intValue();
        this.z = z.intValue();
    }

    public Pos offset(int x, int y, int z) {
        return new Pos(x() + x, y() + y, z() + z);
    }

    public Pos offsetX(int x) {
        return offset(x, 0, 0);
    }

    public Pos offsetY(int y) {
        return offset(0, y, 0);
    }

    public Pos offsetZ(int z) {
        return offset(0, 0, z);
    }

    public Pos offset(WorldDirection direction, int value) {
        switch (direction){
            case UP:
                return offsetY(value);
            case DOWN:
                return offsetY(-value);
            case NORTH:
                return offsetZ(-value);
            case SOUTH:
                return offsetZ(value);
            case WEST:
                return offsetX(-value);
            case EAST:
                return offsetX(value);

            default:
                return this;
        }
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    public int z() {
        return z;
    }

    public Pos3d as3d() {
        return new Pos3d(x, y, z);
    }
}