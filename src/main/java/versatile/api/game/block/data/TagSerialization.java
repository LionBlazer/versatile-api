package versatile.api.game.block.data;

import versatile.api.internal.INSerializationFactory;

import java.io.Serializable;

public interface TagSerialization {
    static TagSerialization base() {
        return INSerializationFactory.serializationBase();
    }

    <T extends Serializable> TagCompound serialize(T object);

    <T extends Serializable> T deserialize(TagCompound data, Class<T> clazz);

    default <T extends Serializable> T deserializeOrNull(TagCompound owner, String name, Class<T> clazz) {
        TagCompound tag = owner.getTagOrDefault(name, null);
        if(tag == null)
            return null;
        return deserialize(tag, clazz);
    }
}