package versatile.api.game.block.data;

/**
 * The minimal serialization/deserialization unit
 */
public interface DataCapacitor {

    /**
     * Deserialize object from <tt>tag</tt>
     */
    void restoreDataFrom(TagCompound tag);


    /**
     * Serialize object in <tt>tag</tt>
     */
    void saveDataTo(TagCompound tag);


    default TagCompound serialize(){
        TagCompound compound = TagCompound.create();
        saveDataTo(compound);
        return compound;
    }

}