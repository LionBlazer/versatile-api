package versatile.api.game.block.data;

import versatile.api.game.block.BlockHandler;

public interface BlockDataHandler extends BlockHandler, DataCapacitor {
}
