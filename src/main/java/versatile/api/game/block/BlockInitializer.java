package versatile.api.game.block;

import versatile.api.game.block.data.DataCapacitor;

import java.util.function.Consumer;
import java.util.function.Supplier;

public interface BlockInitializer {
    void registerBlock(
            BlockInfo source,
            Consumer<BlockFactory> factory
    );

    BlockInfo registerBlock(
            String uniqueName,
            Consumer<BlockFactory> factory
    );

    void registerDataBlock(
            BlockInfo source,
            Consumer<BlockFactory> factory,
            Supplier<DataCapacitor> capacitorAllocator
    );

    BlockInfo registerDataBlock(
            String uniqueName,
            Consumer<BlockFactory> factory,
            Supplier<DataCapacitor> capacitorAllocator
    );
}
