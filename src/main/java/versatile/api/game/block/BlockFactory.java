package versatile.api.game.block;

import versatile.api.game.lang.IDisplayName;
import versatile.api.resource.texture.TextureFactory;

public interface BlockFactory {
    void setDisplayName(IDisplayName displayName);

    void setTexture(TextureFactory texture);

    void setMaterial(BlockMaterial material);

    void setHarvestType(HarvestType type);

    void setHardness(float hardness);

    void setHarvestTier(int tier);

    /**
     * Enable/disable itemblock registration
     * Default: true
     */
    void setCreateItemBlock(boolean createItemBlock);
}
