package versatile.api.game.block;

import versatile.api.IMod;
import versatile.api.internal.INModUtil;

import java.util.Objects;

public final class BlockInfo {
    public static final BlockInfo EMPTY = new BlockInfo("", "");
    private final String name;
    private final String owner;

    public BlockInfo(String owner, String name) {
        Objects.requireNonNull(this.name = name);
        Objects.requireNonNull(this.owner = owner);
    }

    public BlockInfo(IMod owner, String name) {
        Objects.requireNonNull(this.name = name);
        Objects.requireNonNull(this.owner = INModUtil.map(owner).value());
    }

    public BlockInfo(String all) {
        if(!all.contains(":")) throw new RuntimeException("Invalid BlockInfo string: " + all);
        this.owner = all.substring(0, all.indexOf(":"));
        this.name = all.substring(all.indexOf(":") + 1);
    }

    public String getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlockInfo blockInfo = (BlockInfo) o;
        return Objects.equals(name, blockInfo.name) &&
                Objects.equals(owner, blockInfo.owner);
    }

    public boolean isAll(BlockInfo... infos){
        for (BlockInfo b: infos)
            if (!equals(b)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, owner);
    }

    @Override
    public String toString() {
        return owner + ":" + name;
    }

    public boolean isEmpty(){
        return name.isEmpty() && owner.isEmpty();
    }
}
