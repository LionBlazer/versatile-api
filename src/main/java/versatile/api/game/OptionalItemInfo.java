package versatile.api.game;

import versatile.api.game.item.ItemContainer;

public interface OptionalItemInfo extends OptionalInfo {
    default boolean is(ItemContainer container){
        return container.isItemEqual(this);
    }
}
