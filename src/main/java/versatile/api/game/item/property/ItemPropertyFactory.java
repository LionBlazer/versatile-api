package versatile.api.game.item.property;

import versatile.api.game.item.VanillaItem;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public final class ItemPropertyFactory{
    private static Map<String, Function<String, ?>> deserializationMap = new HashMap<>();
    private static Map<Class<?>, String> tagMap = new HashMap<>();

    static {
        registerProperty(ColorType.tagName, ColorType::typeOrDefault, ColorType.class);
        registerProperty(WoodType.tagName, WoodType::typeOrDefault, WoodType.class);
    }

    private static <T extends Enum<?> & VanillaItem.Property> void registerProperty(String tagName, Function<String, T> fromName, Class<T> d){
        deserializationMap.put(tagName, fromName);
        tagMap.put(d, tagName);
    }

    public static VanillaItem.Property getProperty(String tagName, String name){
        return (VanillaItem.Property) deserializationMap.get(tagName).apply(name);
    }

    public static String getTagName(VanillaItem.Property property){
        return tagMap.getOrDefault(property.getClass(), "");
    }
}
