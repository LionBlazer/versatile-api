package versatile.api.game.item.property;

import versatile.api.game.item.VanillaItem;

public enum WoodType implements VanillaItem.Property{
    OAK("oak"),
    SPRUCE("spruce"),
    BIRCH("birch"),
    JUNGLE("jungle"),
    ACACIA("acacia"),
    DARK_OAK("dark_oak"),

    DEFAULT("")
    ;

    private String name;
    public static final String tagName = "wood_type";

    WoodType(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public static WoodType typeOrDefault(String name){
        for (WoodType type: values())
            if(type.getName().equals(name))
                return type;
        return DEFAULT;
    }
}
