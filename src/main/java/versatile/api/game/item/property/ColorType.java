package versatile.api.game.item.property;

import versatile.api.game.item.VanillaItem;

public enum ColorType implements VanillaItem.Property{
    BLACK("black"),
    RED("red"),
    GREEN("green"),
    BROWN("brown"),
    BLUE("blue"),
    PURPLE("purple"),
    CYAN("cyan"),
    LIGHT_GRAY("light_gray"),
    GRAY("gray"),
    PINK("pink"),
    LIME("line"),
    YELLOW("yellow"),
    LIGHT_BLUE("light_blue"),
    MAGENTA("magenta"),
    ORANGE("orange"),
    WHITE("white"),

    DEFAULT("")
    ;

    private String name;
    public static final String tagName = "color_type";

    ColorType(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public static ColorType typeOrDefault(String name){
        for (ColorType type: values())
            if(type.getName().equals(name))
                return type;
        return DEFAULT;
    }
}
