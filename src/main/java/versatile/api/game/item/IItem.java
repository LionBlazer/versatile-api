package versatile.api.game.item;

import versatile.api.game.block.BlockInfo;
import versatile.api.game.lang.IDisplayName;
import versatile.api.resource.texture.TextureFactory;

public interface IItem {
    BlockInfo getSource();

    IDisplayName displayName();

    TextureFactory texture();
}