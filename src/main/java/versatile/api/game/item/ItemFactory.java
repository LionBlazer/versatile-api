package versatile.api.game.item;

import versatile.api.game.lang.IDisplayName;
import versatile.api.resource.texture.TextureFactory;

public interface ItemFactory {
    void setDisplayName(IDisplayName displayName);

    void setTexture(TextureFactory texture);
}
