package versatile.api.game.item;

import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.data.DataCapacitor;
import versatile.api.game.block.data.TagCompound;
import versatile.api.game.item.property.ItemPropertyFactory;

import javax.annotation.Nullable;
import java.util.Objects;
import java.util.Optional;

public final class PropertiedItem implements DataCapacitor, Cloneable{
    private BlockInfo info;

    @Nullable
    private VanillaItem.Property property;

    public PropertiedItem(BlockInfo info, @Nullable VanillaItem.Property property) {
        this(info);
        this.property = property;
    }

    public static PropertiedItem empty(){
        return new PropertiedItem(BlockInfo.EMPTY);
    }

    public PropertiedItem(BlockInfo info) {
        this.info = Objects.requireNonNull(info);
    }

    public BlockInfo getInfo() {
        return info;
    }

    public Optional<VanillaItem.Property> getProperty() {
        return Optional.ofNullable(property);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PropertiedItem that = (PropertiedItem) o;
        return Objects.equals(info, that.info) &&
                Objects.equals(property, that.property);
    }

    @Override
    public int hashCode() {
        return Objects.hash(info, property);
    }

    @Override
    public String toString() {
        return info.toString() + (property != null ? (":" + property.getName()) : "");
    }

    public static PropertiedItem deserialize(TagCompound compound){
        PropertiedItem item = new PropertiedItem(BlockInfo.EMPTY);
        item.restoreDataFrom(compound);
        return item;
    }

    @Override
    public void restoreDataFrom(TagCompound tag) {
        info = tag.getInfoOrEmpty("info");

        if(!tag.contains("property_tag")){
            property = null;
            return;
        }

        String tagName = tag.getStringOrEmpty("property_tag");
        String propertyName = tag.getStringOrEmpty("property_name");
        property = ItemPropertyFactory.getProperty(tagName, propertyName);
    }

    @Override
    public void saveDataTo(TagCompound tag) {
        tag.set("info", info);

        if(property == null) return;

        String tagName = ItemPropertyFactory.getTagName(property);
        if(tagName.isEmpty()) return;

        String propertyName = property.getName();

        tag.set("property_tag", tagName);
        tag.set("property_name", propertyName);
    }

    @Override
    public PropertiedItem clone() {
        try {
            return (PropertiedItem) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}