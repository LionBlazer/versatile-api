package versatile.api.game;

import versatile.api.game.block.VanillaBlock;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface RequiredDependencies {
    VanillaBlock[] value();
}
