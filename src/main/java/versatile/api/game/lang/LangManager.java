package versatile.api.game.lang;

public interface LangManager {
    String fromLangProperties(String key);
    String permanent(String constantName);

    default String permanentFormatted(String constantName){
        if (constantName.isEmpty())
            return "~Empty~";
        String name = constantName.replace('_', ' ').trim();
        return permanent(name.substring(0, 1).toUpperCase() + name.substring(1));
    }
}
