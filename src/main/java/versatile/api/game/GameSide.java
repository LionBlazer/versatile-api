package versatile.api.game;

public enum GameSide {
    SERVER,
    CLIENT,
    ;

    public boolean isClient(){
        return this == CLIENT;
    }

}
