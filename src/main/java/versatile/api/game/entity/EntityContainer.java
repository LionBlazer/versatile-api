package versatile.api.game.entity;

import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.data.DataCapacitor;
import versatile.api.game.world.Pos3d;
import versatile.api.game.block.data.TagCompound;

public interface EntityContainer extends DataCapacitor {
    /**
     * @return {@link BlockInfo}'s type  of this entity. If
     * this parameter will be changed while the entity is
     * not dead, it will be removed from the world and in
     * its place will be a new entity with the same data but
     * a different type. This behavior can be achieved by
     * changing the type in the {@link TagCompound} of entity
     * loaded via the {@link #restoreDataFrom(TagCompound)}
     */
    BlockInfo getType();

    /**
     * @return the current entity position in the world
     */
    Pos3d getPosition();

    /**
     * Set entity position to {@code pos}. Automatically
     * updates all related parameters including on the client
     * @param pos the new entity position
     */
    void setPosition(Pos3d pos);
}
