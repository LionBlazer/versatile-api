package versatile.api.internal;

import versatile.api.game.block.BlockInfo;

import java.util.HashMap;
import java.util.function.Consumer;

/*
* Internal api class
*/
public final class INBlockRegistry {
    private static HashMap<String, BlockInfo> vanillaBlocks;
    private static Consumer<String> loggerUndefined;

    public static void setLoggerUndefined(Consumer<String> loggerUndefined) {
        if(INBlockRegistry.loggerUndefined != null)
            throw new RuntimeException("Logger already initialized!");
        INBlockRegistry.loggerUndefined = loggerUndefined;
    }

    public static Consumer<String> getLoggerUndefined() {
        if(loggerUndefined == null)
            return s -> {};
        return loggerUndefined;
    }

    public static void initVanillaBlocks(HashMap<String, BlockInfo> vanillaBlocks){
        if(INBlockRegistry.vanillaBlocks == null)
            INBlockRegistry.vanillaBlocks = vanillaBlocks;
        else
            throw new RuntimeException("Blocks already initialized!");
    }

    public static HashMap<String, BlockInfo> vanillaBlocks() {
        if(INBlockRegistry.vanillaBlocks == null)
            throw new RuntimeException("Blocks were not initialized!");
        return vanillaBlocks;
    }
}
