package versatile.api.internal;

import versatile.api.game.block.BlockInfo;

import java.util.HashMap;

/*
* Internal api class
*/
public final class INEntityRegistry {
    private static HashMap<String, BlockInfo> vanillaEntities;

    public static void initVanillaEntities(HashMap<String, BlockInfo> vanillaEntities){
        if(INEntityRegistry.vanillaEntities == null)
            INEntityRegistry.vanillaEntities = vanillaEntities;
        else
            throw new RuntimeException("Entities already initialized!");
    }

    public static HashMap<String, BlockInfo> vanillaEntities() {
        if(INEntityRegistry.vanillaEntities == null)
            throw new RuntimeException("Entities were not initialized!");
        return vanillaEntities;
    }
}
