package versatile.api.internal;

import versatile.api.game.block.BlockInfo;

import java.util.HashMap;
import java.util.function.Consumer;

/*
* Internal api class
*/
public final class INItemRegistry {
    private static HashMap<String, BlockInfo> vanillaItems;
    private static Consumer<String> loggerUndefined;

    public static void setLoggerUndefined(Consumer<String> loggerUndefined) {
        if(INItemRegistry.loggerUndefined != null)
            throw new RuntimeException("Logger already initialized!");
        INItemRegistry.loggerUndefined = loggerUndefined;
    }

    public static Consumer<String> getLoggerUndefined() {
        if(loggerUndefined == null)
            return s -> {};
        return loggerUndefined;
    }
    public static void initVanillaItems(HashMap<String, BlockInfo> vanillaItems){
        if(INItemRegistry.vanillaItems == null)
            INItemRegistry.vanillaItems = vanillaItems;
        else
            throw new RuntimeException("Items already initialized!");
    }

    public static HashMap<String, BlockInfo> vanillaItems() {
        if(INItemRegistry.vanillaItems == null)
            throw new RuntimeException("Items were not initialized!");
        return vanillaItems;
    }
}
