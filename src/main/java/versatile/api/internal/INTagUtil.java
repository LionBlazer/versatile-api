package versatile.api.internal;

import versatile.api.game.block.data.TagCompound;

import java.util.function.Supplier;

/*
* Internal api class
*/
public class INTagUtil {
    private static Supplier<TagCompound> emptyTagFactory;


    public static void initEmptyTagFactory(Supplier<TagCompound> factory) {
        if(emptyTagFactory == null)
            emptyTagFactory = factory;
        else
            throw new RuntimeException("Tag factory already initialized!");
    }

    public static TagCompound emptyTag() {
        if(emptyTagFactory == null)
            throw new RuntimeException("Tag factory was not initialized!");
        return emptyTagFactory.get();
    }
}
