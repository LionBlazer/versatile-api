package versatile.api.internal;

import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.data.TagCompound;
import versatile.api.game.item.ItemContainer;
import versatile.api.game.item.PropertiedItem;
import versatile.api.game.item.VanillaItem;
import versatile.api.game.item._INItemWrapper;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

/*
 * Internal api class
 */
public class INItemUtil {
    private static Map<BlockInfo, VanillaItem.Property[]> propertiesMap;
    private static Function<PropertiedItem, Set<PropertiedItem>> aliasesCallback;


    public static void initAliasesCallback(Function<PropertiedItem, Set<PropertiedItem>> aliasesCallback) {
        if(INItemUtil.aliasesCallback != null)
            throw new RuntimeException("Aliases callback already initialized");
        INItemUtil.aliasesCallback = aliasesCallback;
    }


    public static void initPropertiesMap(Map<BlockInfo, VanillaItem.Property[]> propertiesMap) {
        if(INItemUtil.propertiesMap != null)
            throw new RuntimeException("Properties map already initialized");
        INItemUtil.propertiesMap = propertiesMap;
    }

    public static VanillaItem.Property getDefaultProperty(BlockInfo info){
        if(propertiesMap == null)
            throw new RuntimeException("Properties map was not initialized!");
        VanillaItem.Property[] orDefault = propertiesMap.getOrDefault(info, new VanillaItem.Property[]{});
        if(orDefault.length == 0)
            return null;
        return orDefault[0];
    }

    public static void linkItemToCallback(ItemContainer container, INItemCallback callback) {
        _INItemWrapper.setCallBack(container, callback);
    }

    public interface INItemCallback {

        void onStackSizeChanged(ItemContainer container, int count);

        void onDataTagChanged(ItemContainer container, TagCompound newData);


        int getActualCount(ItemContainer container);

        TagCompound getActualTag(ItemContainer container, TagCompound data);

        default boolean allowChangeItemType(ItemContainer container) {
            return false;
        }
    }


    @Nullable
    public static Set<PropertiedItem> getAliases(PropertiedItem info){
        return aliasesCallback.apply(info);
    }
}
