package versatile.api.resource.texture;

import java.awt.image.BufferedImage;

public interface RenderTexture {
    String uniqueName();

    boolean makeSuitable();

    BufferedImage image();
}
