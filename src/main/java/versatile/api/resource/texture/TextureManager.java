package versatile.api.resource.texture;

import java.awt.*;
import java.net.URL;

public interface TextureManager {
    RenderTexture loadTextureFromResource(String path);
    RenderTexture loadTextureFromURL(URL path);
    RenderTexture loadTextureFromColor(Color color);
}
