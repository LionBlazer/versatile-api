package versatile.api.render;

import versatile.api.resource.model.RenderModel;

public interface IModelRender extends IRender{
    RenderModel model();


    @Override
    default void render(){
        model().render();
    }
}
