package versatile.test;

import net.minecraft.client.gui.GuiMainMenu;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod(modid = "tests_mod", name = "For tests only mod")
public class TestMod {

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void event(GuiOpenEvent event) {
        if (event.getGui() instanceof GuiMainMenu && VersatileTestRunner.latch.getCount() > 0) {
            VersatileTestRunner.latch.countDown();
        }
    }
}
